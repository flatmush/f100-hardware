EESchema Schematic File Version 4
LIBS:f100-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Top-level, Buttons & Misc."
Date "2019-05-29"
Rev "1.3.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW3
U 1 1 5C6330E0
P 1400 5400
F 0 "SW3" H 1400 5685 50  0000 C CNN
F 1 "UP" H 1400 5594 50  0000 C CNN
F 2 "Footprints:Button_DPAD_5.2mm" H 1400 5600 50  0001 C CNN
F 3 "" H 1400 5600 50  0001 C CNN
	1    1400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5400 1200 5400
$Comp
L Switch:SW_Push SW7
U 1 1 5C63DCC2
P 2600 6300
F 0 "SW7" H 2600 6585 50  0000 C CNN
F 1 "A" H 2600 6494 50  0000 C CNN
F 2 "Footprints:Button_Circle_5.6mm" H 2600 6500 50  0001 C CNN
F 3 "" H 2600 6500 50  0001 C CNN
	1    2600 6300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 6300 2400 6300
Wire Wire Line
	1600 5400 1700 5400
$Comp
L Switch:SW_Push SW11
U 1 1 5C6458FB
P 3800 5400
F 0 "SW11" H 3800 5685 50  0000 C CNN
F 1 "HOME" H 3800 5594 50  0000 C CNN
F 2 "Footprints:Button_Rectangle_9.3x4.4mm" H 3800 5600 50  0001 C CNN
F 3 "" H 3800 5600 50  0001 C CNN
	1    3800 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 5400 3600 5400
$Comp
L Switch:SW_Push SW4
U 1 1 5C653C94
P 1400 5850
F 0 "SW4" H 1400 6135 50  0000 C CNN
F 1 "DOWN" H 1400 6044 50  0000 C CNN
F 2 "Footprints:Button_DPAD_5.2mm" H 1400 6050 50  0001 C CNN
F 3 "" H 1400 6050 50  0001 C CNN
	1    1400 5850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1100 5850 1200 5850
$Comp
L Switch:SW_Push SW8
U 1 1 5C653C9F
P 2600 6750
F 0 "SW8" H 2600 7035 50  0000 C CNN
F 1 "B" H 2600 6944 50  0000 C CNN
F 2 "Footprints:Button_Circle_5.6mm" H 2600 6950 50  0001 C CNN
F 3 "" H 2600 6950 50  0001 C CNN
	1    2600 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 5850 1700 5850
$Comp
L Switch:SW_Push SW12
U 1 1 5C653CAD
P 3800 5850
F 0 "SW12" H 3800 6135 50  0000 C CNN
F 1 "VOLUME" H 3800 6044 50  0000 C CNN
F 2 "Footprints:Button_Rectangle_9.3x4.4mm" H 3800 6050 50  0001 C CNN
F 3 "" H 3800 6050 50  0001 C CNN
	1    3800 5850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 5850 3600 5850
Wire Wire Line
	1100 5400 1100 5850
Wire Wire Line
	3500 5400 3500 5850
$Comp
L Switch:SW_Push SW5
U 1 1 5C668746
P 1400 6300
F 0 "SW5" H 1400 6585 50  0000 C CNN
F 1 "LEFT" H 1400 6494 50  0000 C CNN
F 2 "Footprints:Button_DPAD_5.2mm" H 1400 6500 50  0001 C CNN
F 3 "" H 1400 6500 50  0001 C CNN
	1    1400 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 6300 1200 6300
Wire Wire Line
	1600 6300 1700 6300
$Comp
L Switch:SW_Push SW13
U 1 1 5C66875B
P 3800 6300
F 0 "SW13" H 3800 6585 50  0000 C CNN
F 1 "SELECT" H 3800 6494 50  0000 C CNN
F 2 "Footprints:Button_Rectangle_9.3x4.4mm" H 3800 6500 50  0001 C CNN
F 3 "" H 3800 6500 50  0001 C CNN
	1    3800 6300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 6300 3600 6300
Wire Wire Line
	1100 5850 1100 6300
Connection ~ 1100 5850
Wire Wire Line
	3500 5850 3500 6300
Connection ~ 3500 5850
$Comp
L Switch:SW_Push SW6
U 1 1 5C684252
P 1400 6750
F 0 "SW6" H 1400 7035 50  0000 C CNN
F 1 "RIGHT" H 1400 6944 50  0000 C CNN
F 2 "Footprints:Button_DPAD_5.2mm" H 1400 6950 50  0001 C CNN
F 3 "" H 1400 6950 50  0001 C CNN
	1    1400 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1100 6750 1200 6750
Wire Wire Line
	1600 6750 1700 6750
$Comp
L Switch:SW_Push SW14
U 1 1 5C684267
P 3800 6750
F 0 "SW14" H 3800 7035 50  0000 C CNN
F 1 "START" H 3800 6944 50  0000 C CNN
F 2 "Footprints:Button_Rectangle_9.3x4.4mm" H 3800 6950 50  0001 C CNN
F 3 "" H 3800 6950 50  0001 C CNN
	1    3800 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 6750 3600 6750
Wire Wire Line
	3500 6300 3500 6750
Connection ~ 3500 6300
Wire Wire Line
	1100 6300 1100 6750
Connection ~ 1100 6300
$Comp
L Mechanical:MountingHole MH1
U 1 1 5CFDD3B6
P 3700 3900
F 0 "MH1" H 3800 3946 50  0000 L CNN
F 1 "1.9mm" H 3800 3855 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 3700 3900 50  0001 C CNN
F 3 "~" H 3700 3900 50  0001 C CNN
	1    3700 3900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5CFDD6C2
P 4700 3950
F 0 "MH2" H 4800 3996 50  0000 L CNN
F 1 "1.9mm" H 4800 3905 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 4700 3950 50  0001 C CNN
F 3 "~" H 4700 3950 50  0001 C CNN
	1    4700 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH3
U 1 1 5CFE1AC0
P 3700 4200
F 0 "MH3" H 3800 4246 50  0000 L CNN
F 1 "1.9mm" H 3800 4155 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 3700 4200 50  0001 C CNN
F 3 "~" H 3700 4200 50  0001 C CNN
	1    3700 4200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5CFE5E3B
P 4200 4200
F 0 "MH4" H 4300 4246 50  0000 L CNN
F 1 "1.9mm" H 4300 4155 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 4200 4200 50  0001 C CNN
F 3 "~" H 4200 4200 50  0001 C CNN
	1    4200 4200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH5
U 1 1 5CFEA185
P 4700 4200
F 0 "MH5" H 4800 4246 50  0000 L CNN
F 1 "1.9mm" H 4800 4155 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 4700 4200 50  0001 C CNN
F 3 "~" H 4700 4200 50  0001 C CNN
	1    4700 4200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH8
U 1 1 5CFF2825
P 4700 4450
F 0 "MH8" H 4800 4496 50  0000 L CNN
F 1 "1.9mm" H 4800 4405 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 4700 4450 50  0001 C CNN
F 3 "~" H 4700 4450 50  0001 C CNN
	1    4700 4450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH7
U 1 1 5CFF6B77
P 4200 4450
F 0 "MH7" H 4300 4496 50  0000 L CNN
F 1 "1.9mm" H 4300 4405 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 4200 4450 50  0001 C CNN
F 3 "~" H 4200 4450 50  0001 C CNN
	1    4200 4450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH6
U 1 1 5CFFAED7
P 3700 4550
F 0 "MH6" H 3800 4596 50  0000 L CNN
F 1 "1.9mm" H 3800 4505 50  0000 L CNN
F 2 "Footprints:MountingHole_1.9mm_M1.8" H 3700 4550 50  0001 C CNN
F 3 "~" H 3700 4550 50  0001 C CNN
	1    3700 4550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH9
U 1 1 5D008836
P 5600 3950
F 0 "MH9" H 5700 3996 50  0000 L CNN
F 1 "1.5mm" H 5700 3905 50  0000 L CNN
F 2 "Footprints:MountingHole_1.5mm_M1.4" H 5600 3950 50  0001 C CNN
F 3 "~" H 5600 3950 50  0001 C CNN
	1    5600 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH10
U 1 1 5D008970
P 6100 3950
F 0 "MH10" H 6200 3996 50  0000 L CNN
F 1 "1.5mm" H 6200 3905 50  0000 L CNN
F 2 "Footprints:MountingHole_1.5mm_M1.4" H 6100 3950 50  0001 C CNN
F 3 "~" H 6100 3950 50  0001 C CNN
	1    6100 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH11
U 1 1 5D00CDEE
P 5600 4200
F 0 "MH11" H 5700 4246 50  0000 L CNN
F 1 "2.0mm" H 5700 4155 50  0000 L CNN
F 2 "Footprints:MountingHole_2.0mm" H 5600 4200 50  0001 C CNN
F 3 "~" H 5600 4200 50  0001 C CNN
	1    5600 4200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH12
U 1 1 5D00CF1C
P 5600 4450
F 0 "MH12" H 5700 4496 50  0000 L CNN
F 1 "3.2mm" H 5700 4405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5600 4450 50  0001 C CNN
F 3 "~" H 5600 4450 50  0001 C CNN
	1    5600 4450
	1    0    0    -1  
$EndComp
$Sheet
S 1000 1000 1200 1250
U 5C6A25D2
F0 "mcu" 50
F1 "mcu.sch" 50
$EndSheet
$Sheet
S 2400 1000 1200 1250
U 5C6A6B11
F0 "memory" 50
F1 "memory.sch" 50
$EndSheet
Wire Wire Line
	2300 6750 2400 6750
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO2
U 1 1 5C6E739F
P 2700 4500
F 0 "LOGO2" H 2947 4571 50  0000 L CNN
F 1 "KiCad" H 2947 4480 50  0000 L CNN
F 2 "Footprints:KiCad-Logo_5mm_Exposed_Copper" H 2700 4500 50  0001 C CNN
F 3 "~" H 2700 4500 50  0001 C CNN
	1    2700 4500
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO1
U 1 1 5C6E7436
P 2700 4000
F 0 "LOGO1" H 2947 4071 50  0000 L CNN
F 1 "OpenHardware" H 2947 3980 50  0000 L CNN
F 2 "Footprints:OSHW-Logo2_7.3x6mm_Exposed_Copper" H 2700 4000 50  0001 C CNN
F 3 "~" H 2700 4000 50  0001 C CNN
	1    2700 4000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole FID1
U 1 1 5C6FA509
P 1300 3900
F 0 "FID1" H 1400 3946 50  0000 L CNN
F 1 "Fiducial" H 1400 3855 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Dia_2mm_Outer" H 1300 3900 50  0001 C CNN
F 3 "~" H 1300 3900 50  0001 C CNN
	1    1300 3900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole FID3
U 1 1 5C6FA6CB
P 1900 4200
F 0 "FID3" H 2000 4246 50  0000 L CNN
F 1 "Fiducial" H 2000 4155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Dia_2mm_Outer" H 1900 4200 50  0001 C CNN
F 3 "~" H 1900 4200 50  0001 C CNN
	1    1900 4200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole FID2
U 1 1 5C7052EE
P 1900 3900
F 0 "FID2" H 2000 3946 50  0000 L CNN
F 1 "Fiducial" H 2000 3855 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Dia_2mm_Outer" H 1900 3900 50  0001 C CNN
F 3 "~" H 1900 3900 50  0001 C CNN
	1    1900 3900
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small SCREW3
U 1 1 5C6DA77B
P 1300 3400
F 0 "SCREW3" H 1547 3471 50  0000 L CNN
F 1 "ScrewSymbol" H 1547 3380 50  0000 L CNN
F 2 "Footprints:Symbol_Screw_SilkscreenTop" H 1300 3400 50  0001 C CNN
F 3 "~" H 1300 3400 50  0001 C CNN
	1    1300 3400
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small SCREW1
U 1 1 5C6E1B7A
P 1300 2900
F 0 "SCREW1" H 1547 2971 50  0000 L CNN
F 1 "ScrewSymbol" H 1547 2880 50  0000 L CNN
F 2 "Footprints:Symbol_Screw_SilkscreenTop" H 1300 2900 50  0001 C CNN
F 3 "~" H 1300 2900 50  0001 C CNN
	1    1300 2900
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small SCREW2
U 1 1 5C6E5640
P 2400 2900
F 0 "SCREW2" H 2647 2971 50  0000 L CNN
F 1 "ScrewSymbol" H 2647 2880 50  0000 L CNN
F 2 "Footprints:Symbol_Screw_SilkscreenTop" H 2400 2900 50  0001 C CNN
F 3 "~" H 2400 2900 50  0001 C CNN
	1    2400 2900
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small SCREW4
U 1 1 5C6E56F2
P 2400 3400
F 0 "SCREW4" H 2647 3471 50  0000 L CNN
F 1 "ScrewSymbol" H 2647 3380 50  0000 L CNN
F 2 "Footprints:Symbol_Screw_SilkscreenTop" H 2400 3400 50  0001 C CNN
F 3 "~" H 2400 3400 50  0001 C CNN
	1    2400 3400
	1    0    0    -1  
$EndComp
Text GLabel 1700 5400 2    50   Output ~ 0
BTN_UP
Text GLabel 1700 5850 2    50   Output ~ 0
BTN_DOWN
Text GLabel 1700 6300 2    50   Output ~ 0
BTN_LEFT
Text GLabel 1700 6750 2    50   Output ~ 0
BTN_RIGHT
Text GLabel 2900 6750 2    50   Output ~ 0
BTN_B
Text GLabel 2900 6300 2    50   Output ~ 0
BTN_A
Text GLabel 4100 5400 2    50   Output ~ 0
BTN_HOME
Text GLabel 4100 5850 2    50   Output ~ 0
BTN_VOLUME
Text GLabel 4100 6300 2    50   Output ~ 0
BTN_SELECT
Text GLabel 4100 6750 2    50   Output ~ 0
BTN_START
Wire Wire Line
	2800 6300 2900 6300
Wire Wire Line
	2900 6750 2800 6750
Wire Wire Line
	4000 5850 4100 5850
Wire Wire Line
	4100 5400 4000 5400
Wire Wire Line
	4100 6300 4000 6300
Wire Wire Line
	4100 6750 4000 6750
$Comp
L power:GND #PWR0165
U 1 1 5D1FEA74
P 1100 6850
F 0 "#PWR0165" H 1100 6600 50  0001 C CNN
F 1 "GND" H 1105 6677 50  0000 C CNN
F 2 "" H 1100 6850 50  0001 C CNN
F 3 "" H 1100 6850 50  0001 C CNN
	1    1100 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0166
U 1 1 5D1FEB35
P 3500 6850
F 0 "#PWR0166" H 3500 6600 50  0001 C CNN
F 1 "GND" H 3505 6677 50  0000 C CNN
F 2 "" H 3500 6850 50  0001 C CNN
F 3 "" H 3500 6850 50  0001 C CNN
	1    3500 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0167
U 1 1 5D1FEBF6
P 2300 6850
F 0 "#PWR0167" H 2300 6600 50  0001 C CNN
F 1 "GND" H 2305 6677 50  0000 C CNN
F 2 "" H 2300 6850 50  0001 C CNN
F 3 "" H 2300 6850 50  0001 C CNN
	1    2300 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 6750 1100 6850
Connection ~ 1100 6750
Wire Wire Line
	2300 6850 2300 6750
Wire Wire Line
	3500 6850 3500 6750
Connection ~ 3500 6750
$Sheet
S 3800 1000 1200 1250
U 5C8D651F
F0 "audio_video" 50
F1 "audio_video.sch" 50
$EndSheet
$Sheet
S 5200 1000 1200 1250
U 5C830E1F
F0 "power" 50
F1 "power.sch" 50
$EndSheet
Connection ~ 2300 6750
Wire Wire Line
	2300 6750 2300 6300
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO3
U 1 1 5CC9EE2A
P 2700 5000
F 0 "LOGO3" H 2947 5071 50  0000 L CNN
F 1 "Flatmush" H 2947 4980 50  0000 L CNN
F 2 "Footprints:Flatmush" H 2700 5000 50  0001 C CNN
F 3 "~" H 2700 5000 50  0001 C CNN
	1    2700 5000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
